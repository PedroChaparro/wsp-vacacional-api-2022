FROM node:alpine3.16

RUN mkdir -p /usr/src/app

RUN apk add --no-cache bash

WORKDIR /usr/srs/app

COPY . .

RUN npm install

EXPOSE 3000

CMD [ "npm", "run", "dev" ]