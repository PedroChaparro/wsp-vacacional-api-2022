import { Request, Response } from 'express';
import { note, user, expansionModule } from '../database/schemas';

const controllers = {
	//Example
	hello: async (_req: Request, res: Response) => {
		const newUser = new user({
			username: 'cualquiera',
			fullname: 'cualuiera',
			mail: 'cualuiera',
			password: 'cualuiera',
			role: 'cualuiera',
		});

		await newUser.save();

		const newNote = new note({
			title: 'Probando',
			content: 'Vacio',
		});

		await newNote.save();

		const newModule = new expansionModule({
			type: 'Sticker',
			title: 'Hola',
		});

		await newModule.save();

		res.send('Sirvio?');
	},
	// #################################
	// Cart routes controllers functions here
	// #################################
};

export default controllers;
