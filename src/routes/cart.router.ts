// #################################
// Cart routes here
// #################################

import { Router } from 'express';
import controllers from '../controllers/cart.controllers';

const router = Router();

// Example
router.get('/hello', controllers.hello);

export default router;
