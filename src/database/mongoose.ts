import mongoose from 'mongoose';

mongoose
	.connect('mongodb://mongo/test')
	.then(() => {
		console.log('Database was connected');
	})
	.catch((err) => {
		console.error(err);
	});
