import { Schema, model } from 'mongoose';

const expansionModuleSchema = new Schema({
	type: {
		type: String,
		required: true,
	},
	title: {
		type: String,
		required: true,
	},
	content: [
		{
			type: String,
		},
	],
	price: {
		type: Number,
	},
});

const expansionModule = model('module', expansionModuleSchema);

const userSchema = new Schema({
	username: {
		type: String,
		required: true,
		unique: true,
	},
	mail: {
		type: String,
		required: true,
		unique: true,
	},
	fullname: {
		type: String,
		required: true,
	},
	password: {
		type: String,
		required: true,
	},
	role: {
		type: String,
		required: true,
	},
	notes: [],
	sharedWithNotes: [
		{
			noteId: Schema.Types.ObjectId,
			ownerId: Schema.Types.ObjectId,
		},
	],
	currentPlan: Schema.Types.ObjectId,
	inventory: [
		{
			type: Schema.Types.ObjectId,
			ref: module,
		},
	],
	cart: [
		{
			type: Schema.Types.ObjectId,
			ref: module,
		},
	],
});

const user = model('user', userSchema);

// friends: [{ type : ObjectId, ref: 'User' }],

const noteSchema = new Schema(
	{
		title: String,
		content: String,
		owner: {
			type: Schema.Types.ObjectId,
			ref: user,
		},
		sharedWith: [
			{
				type: Schema.Types.ObjectId,
				ref: user,
			},
		],
	},
	{
		timestamps: true,
	}
);

const note = model('note', noteSchema);

export { user, note, expansionModule };
