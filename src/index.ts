import express, { Application } from 'express';
import cartRouter from './routes/cart.router';
import './database/mongoose';

const app: Application = express();

//Server config
app.set('port', process.env.PORT || 3000);

//Server Middlewares
app.use(express.json());

//Routes
app.use('/cart/', cartRouter);

//Start the server
app.listen(app.get('port'), () => {
	console.log(`Server listening on localhost:${app.get('port')}`);
});
